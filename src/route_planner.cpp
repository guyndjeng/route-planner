#include "route_planner.h"
#include <algorithm>
#include <iostream>

RoutePlanner::RoutePlanner(RouteModel &model, float start_x, float start_y, float end_x, float end_y): m_Model(model) {
    // Convert inputs to percentage:
    start_x *= 0.01;
    start_y *= 0.01;
    end_x *= 0.01;
    end_y *= 0.01;
    
    this->start_node = &model.FindClosestNode(start_x, start_y);
    this->end_node = &model.FindClosestNode(end_x, end_y);
}


//Heuristic Value
float RoutePlanner::CalculateHValue(RouteModel::Node const *node) {
    return this->end_node->distance(*node);
}

//Compare node
bool CompareNode(RouteModel::Node *first_node, RouteModel::Node *second_node) {
    return first_node->g_value + first_node->h_value >  second_node->g_value + second_node->h_value;
}


//Nearest Neighbors using the OpenStreetMap library
void RoutePlanner::AddNeighbors(RouteModel::Node *current_node) {
    current_node->FindNeighbors();
    for (auto &neighbor : current_node->neighbors) {
        neighbor->parent = current_node;
        neighbor->h_value = CalculateHValue(neighbor);
        neighbor->g_value = current_node->g_value + current_node->distance(*neighbor);
 
        //Adding the neighbor the to open list and set the visited attribute to true
        open_list.push_back(neighbor);
        neighbor->visited = true;
    }
}



RouteModel::Node *RoutePlanner::NextNode() {
   std::sort(this->open_list.begin(), this->open_list.end(), CompareNode);
   auto next_node = this->open_list.back();
   this->open_list.pop_back();
   return next_node;
}


std::vector<RouteModel::Node> RoutePlanner::ConstructFinalPath(RouteModel::Node *current_node) {
    // Create path_found vector
    distance = 0.0f;
    std::vector<RouteModel::Node> path_found;

    while(current_node != this->start_node) {
        distance += current_node->distance(*current_node->parent); 
        path_found.push_back(*current_node);  
        current_node = current_node->parent;
    }
    path_found.push_back(*current_node); // adding the start_node to the back
    distance *= m_Model.MetricScale(); // Multiply the distance by the scale of the map to get meters.
    std::reverse(path_found.begin(), path_found.end());
    return path_found;
}

void RoutePlanner::AStarSearch() {
    RouteModel::Node *current_node = nullptr;
    
    this->open_list.push_back(this->start_node);
    this->start_node->visited=true;
    
    while(this->open_list.size() > 0) {
       current_node =  NextNode();  
       if(current_node == this->end_node){
          m_Model.path = ConstructFinalPath(current_node);
          return;
       } else {
           AddNeighbors(current_node);
       }
    }
}